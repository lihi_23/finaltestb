<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Candidate;
use App\User;
use App\Status;
use App\Role;
use App\Department;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Session;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        
        $roles = Role::all();
        return view('users.index', compact('users', 'roles'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function changedepartment(Request $request){
        $uid = $request->id;
        $did = $request->department_id;
        $user = User::findOrFail($uid);
    
            $from = $user->department->id;
            $user->department_id = $did;
            $user->save();
       
        return redirect('users');

    }




    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::findOrFail($id);
        $departments = Department::all();
        return view('users.show', compact('user','departments'));  

    }
    /**public function auth($id)
    {
       
        $user = User::findOrFail($id);
        $roles= Role::all();
        return view('users.auth', compact('user','roles'));  

    }*/

    
    public function makemanger($uid, $rid = null){
        Gate::authorize('make-manger');
        $user = User::findOrFail($cid);
        $user->role_id = $rid;
        $user->save(); 
        return back();
        //return redirect('candidates');
        //comment
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Gate::authorize('delete-user');
        $user = User::findOrFail($id);
        $user->delete(); 
        return redirect('users'); 
    }
}
