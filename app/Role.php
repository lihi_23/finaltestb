<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Role extends Model
{
   public function users(){
      return $this->belongsToMany('App\User','userroles');        
   }

   public static function userrole($user_id){
      $userroles = DB::table('userroles')->where('user_id',$user_id)->pluck('role_id');
      return self::find($userroles)->all(); 
  }

}

