@extends('layouts.app')

@section('title', 'Details')

@section('content')


<h1>User's Details</h1>
<form method = "post" action = "{{action('UsersController@show', $user->id)}}">
        @csrf 
        <div class="form-group">
            <label for = "name">User name:</label>
            {{$user->name}}
        </div>     
        <div class="form-group">
            <label for = "email">User email:</label>
            {{$user->email}}
        </div>
        
        
        <div> 
            <label for="department_id">User Department:</label>
            
            @if(Gate::allows('change-department'))
                <select class="form-control" name="department_id">                                                                         
                    @foreach ($departments as $department)
                        <option value="{{ $department->id }}"> 
                            {{ $department->name }} 
                        </option>
                    @endforeach    
                </select>
            @else
                {{$user->department->name}}
            @endif
        </div>
        
        <div>
            <input type = "submit" name = "submit" value = "change">
        </div> 
 
                      
        </form>    
@endsection

