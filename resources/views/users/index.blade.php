@extends('layouts.app')

@section('title', 'Users')

@section('content')


<h1>List of users</h1>
<table class = "table table-striped ">
    <tr>
        <th>id</th><th>Name</th><th>Email</th><th>Department</th><th>Delete</th><th>Details</th><th>Auth</th><th>Make Manger</th>
    </tr>
    <!-- the table data -->
    @foreach($users as $user)
        <tr>
            <td>{{$user->id}}</td>
            <td>{{$user->name}}</td>
            <td>{{$user->email}}</td>
            <td>{{$user->department->name}}</td>
            <td><a href = "{{route('user.delete',$user->id)}}">Delete</a></td>
            <td><a href = "{{route('users.show',$user->id)}}">Details</a></td>
            <td>   <select class="form-control" name="role_id"> 
                        @foreach(App\Role::userrole($user->id) as $role)
                        <option value="{{ $role->id}}"> 
                           {{$role->name}}
                        </option>
                        @endforeach  
                      </td>
        
            <td> <button class="btn btn-secondary" type="button" > Make Manger</button></td>
            
        </tr>
    
    @endforeach
</table>
@endsection

